#![allow(dead_code)]

use std::ffi::CStr;
use tizen::elementary;
use std::ops::{Deref, DerefMut};
use std::os::raw::c_void;
use std::mem::uninitialized;

pub use tizen::elementary::Evas_Coord;

pub struct EvasObject {
    obj: *mut elementary::Eo,
    smart_callbacks: HashMap<'static CStr, Box<Fn<(&mut EvasObject,
                                                   *mut c_void)>>>;
}

impl EvasObject {
    fn eo(&mut self) -> *mut elementary::Eo {
        self.obj
    }

    fn geometry_get(&mut self) -> (Evas_Coord, Evas_Coord, Evas_Coord, Evas_Coord) {
        unsafe {
            let mut x = uninitialized();
            let mut y = uninitialized();
            let mut w = uninitialized();
            let mut h = uninitialized();
            elementary::evas_object_geometry_get(self.eo(),
                                                 &mut x, &mut y,
                                                 &mut w, &mut h );
            (x, y, w, h)
        }
    }

    pub fn geometry_set(&mut self,
                        x: Evas_Coord, y: Evas_Coord,
                        w: Evas_Coord, h: Evas_Coord) {
        unsafe { elementary::evas_object_geometry_set(self.eo(), x, y, w, h) }
    }

    pub fn show(&mut self) {
        unsafe { elementary::evas_object_show(self.eo()) }
    }

    pub fn hide(&mut self) {
        unsafe { elementary::evas_object_hide(self.eo()) }
    }

    pub fn size_hint_weight_set(&mut self, x: f64, y: f64) {
        unsafe { elementary::evas_object_size_hint_weight_set(self.eo(), x, y) }
    }

    /*
    pub fn smart_callback_add(&mut self,
                              event: &'static CStr,
                                     func: fn(&mut EvasObject, &'a T),
                                     data: &'a T) {
        unsafe { evas_object_smart_callback_add(self.eo(), event.as_ptr(),
                                                smart_callback_wrapper
    }*/

    /*
    pub fn evas_object_smart_callback_add(obj: *mut Evas_Object,
                                          event:
                                              *const ::std::os::raw::c_char,
                                          func: Evas_Smart_Cb,
                                          data:
                                             *const ::std::os::raw::c_void);

    */
}

/*
extern fn smart_callback_wrapper(data: c_void, obj: *mut elementary::Eo, event_info: *mut c_void) {
}
    ::std::option::Option<unsafe extern "C" fn(data:
                                                   *mut ::std::os::raw::c_void,
                                               obj: *mut Evas_Object,
                                               event_info:
                                                   *mut ::std::os::raw::c_void)>;
*/

pub struct ElmWin(EvasObject);

impl Deref for ElmWin {
    type Target = EvasObject;

    fn deref(&self) -> &EvasObject {
        &self.0
    }
}

impl DerefMut for ElmWin {
    fn deref_mut(&mut self) -> &mut EvasObject {
        &mut self.0
    }
}

impl ElmWin {
    pub fn standard_add(name: &'static CStr, title: &'static CStr) -> Option<Self> {
        let win = unsafe { elementary::elm_win_util_standard_add(name.as_ptr(), title.as_ptr()) };
        if win.is_null() {
            None
        } else {
            Some(ElmWin(EvasObject(win)))
        }
    }

    pub fn autodel_set(&mut self, autodel: bool) {
        unsafe { elementary::elm_win_autodel_set(self.eo(), if autodel { 1 } else { 0 }); }
    }

    pub fn autodel_get(&mut self) -> bool {
        unsafe { elementary::elm_win_autodel_get(self.eo()) != 0 }
    }

    pub fn lower(&mut self) {
        unsafe { elementary::elm_win_lower(self.eo()) }
    }

    pub fn wm_rotation_supported_get(&self) -> bool {
        unsafe { elementary::elm_win_wm_rotation_supported_get(self.eo()) != 0 }
    }
}
