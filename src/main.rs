extern crate curl;

mod tizen;
mod uiapp;
mod evas_object;

use std::process::exit;
use uiapp::UIApp;
use tizen::app::app_control_h;
use evas_object::{EvasObject, ElmWin};
use std::ffi::CStr;

const PACKAGE: &str = "com.ids1024.hue\0";

struct HueApp;

impl UIApp for HueApp {
    fn create(&self) -> bool {
        let package = unsafe { CStr::from_bytes_with_nul_unchecked(PACKAGE.as_bytes()) };
        let mut win = match ElmWin::standard_add(package, package) {
            Some(win) => win,
            None => { return false }
        };
        win.autodel_set(true);
        if win.wm_rotation_supported_get() {
            // TODO
        }
        
        true
    }
    fn terminate(&self) {}
    fn pause(&self) {}
    fn resume(&self) {}
    fn control(&self, _app_control: app_control_h) {} 
}

fn main() {
    let mut app = HueApp;
    exit(app.main())
}
