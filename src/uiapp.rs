#![allow(dead_code)]

use tizen::app::{ui_app_lifecycle_callback_s, ui_app_main, app_control_h};
use std::os::raw::{c_int, c_void, c_char};
use std::marker::Sized;
use std::env::args_os;
use std::os::unix::ffi::OsStrExt;
use std::ptr::null_mut;

pub trait UIApp: Sized {
    fn create(&self) -> bool;
    fn terminate(&self);
    fn pause(&self);
    fn resume(&self);
    fn control(&self, app_control: app_control_h);

    fn main(&mut self) -> c_int {
        let args = args_os().collect::<Vec<_>>();
        let mut argv: Vec<*mut u8> = Vec::new();
        for mut i in args {
            i.push("\0");
            argv.push(i.as_bytes().as_ptr() as *mut c_char);
        }
        argv.push(null_mut());

        let mut event_callback = ui_app_lifecycle_callback_s {
            create: Some(app_create::<Self>),
            terminate: Some(app_terminate::<Self>),
            pause: Some(app_pause::<Self>),
            resume: Some(app_resume::<Self>),
            app_control: Some(app_control::<Self>)
        };

        unsafe { ui_app_main(argv.len() as c_int, argv.as_mut_slice().as_mut_ptr(),
                             &mut event_callback, self as *mut _ as *mut c_void) }
    }
}

extern fn app_create<T: UIApp>(data: *mut c_void) -> bool {
    let app = unsafe { &*(data as *mut T) };
    app.create()
}

extern fn app_terminate<T: UIApp>(data: *mut c_void) {
    let app = unsafe { &*(data as *mut T) };
    app.terminate()
}

extern fn app_pause<T: UIApp>(data: *mut c_void) {
    let app = unsafe { &*(data as *mut T) };
    app.pause()
}

extern fn app_resume<T: UIApp>(data: *mut c_void) {
    let app = unsafe { &*(data as *mut T) };
    app.resume()
}

extern fn app_control<T: UIApp>(app_control: app_control_h, data: *mut c_void) {
    let app = unsafe { &*(data as *mut T) };
    app.control(app_control)
}
