NAME=hue
VERSION = 1.0.0
TIZEN_STUDIO=/home/ian/Devel/tizen-studio
TIZEN_VERSION=3.0
ROOTSTRAP=wearable-3.0-device.core
TRIPLE=arm-linux-gnueabi
RUST_TRIPLE=arm-unknown-linux-gnueabi
SECURITY_PROFILE=ids1024
PKGNAME = com.ids1024.$(NAME)
TOOLCHAIN=gcc-4.9
SRC=src/main.rs src/tizen/mod.rs src/uiapp.rs src/evas_object.rs

PACKAGE = $(PKGNAME)-$(VERSION)-arm.tpk
SYSROOT=$(TIZEN_STUDIO)/platforms/tizen-$(TIZEN_VERSION)/wearable/rootstraps/$(ROOTSTRAP)

export PATH:=$(TIZEN_STUDIO)/tools/$(TRIPLE)-$(TOOLCHAIN)/bin:$(PATH)
export PKG_CONFIG_SYSROOT_DIR=$(SYSROOT)
export PKG_CONFIG_LIBDIR=$(SYSROOT)/usr/lib/pkgconfig
export PKG_CONFIG_PATH=
export PKG_CONFIG_ALLOW_CROSS=1
export RUSTFLAGS=-C link-args=--sysroot=$(SYSROOT)

all: package-debug

run:
	tizen run -p $(PKGNAME)

debug: Debug/$(NAME)

release: Release/$(NAME)

package-debug: Debug/$(PACKAGE)

package-release: Release/$(PACKAGE)

install-debug: Debug/$(PACKAGE)
	tizen install -n $(PACKAGE) -- ./Debug

install-release: Release/$(PACKAGE)
	tizen install -n $(PACKAGE) -- ./Release

Debug/$(PACKAGE): Debug/$(NAME) Debug/build.info
	tizen package -t tpk -s $(SECURITY_PROFILE) -- ./Debug

Release/$(PACKAGE): Release/$(NAME) Release/build.info
	tizen package -S on -t tpk -s $(SECURITY_PROFILE) -- ./Release

Debug/build.info Release/build.info:
	echo project-path=$(PWD) > $@
	echo profile=Wearable >> $@
	echo profile-version=$(TIZEN_VERSION) >> $@
	echo type=app >> $@
	echo config=Debug >> $@
	echo toolchain=$(TOOLCHAIN) >> $@
	echo architecture=armel >> $@

Debug/$(NAME): target/$(RUST_TRIPLE)/debug/$(NAME)
	mkdir -p Debug
	cp $< $@

Release/$(NAME): target/$(RUST_TRIPLE)/release/$(NAME)
	mkdir -p Release
	cp $< $@

target/$(RUST_TRIPLE)/debug/$(NAME): $(SRC)
	cargo build --target=$(RUST_TRIPLE)

target/$(RUST_TRIPLE)/release/$(NAME): $(SRC)
	cargo build --target=$(RUST_TRIPLE) --release

src/tizen/mod.rs: src/tizen/app.rs src/tizen/system_settings.rs src/tizen/dlog.rs src/tizen/elementary.rs
	echo '#![allow(non_camel_case_types)]' > $@
	echo '#![allow(non_snake_case)]' >> $@
	echo '#![allow(non_upper_case_globals)]' >> $@
	echo '#![allow(dead_code)]' >> $@
	echo $(foreach i,$(notdir $(^:.rs=)),"pub mod $i;") >> $@

src/tizen/app.rs:
	mkdir -p src/tizen
	bindgen $(SYSROOT)/usr/include/appfw/app.h --whitelist-function '(ui_)?app_.*' \
                                                   --whitelist-type "(ui_)?app_.*" \
        					   --whitelist-var "(UI_)?APP_.*" \
						   -- --sysroot=$(SYSROOT) -I $(SYSROOT)/usr/include/appfw > $@

src/tizen/system_settings.rs:
	mkdir -p src/tizen
	bindgen $(SYSROOT)/usr/include/system/system_settings.h \
	                                           --whitelist-function 'system_.*' \
                                                   --whitelist-type "system_.*" \
        					   --whitelist-var "SYSTEM_.*" \
						   -- --sysroot=$(SYSROOT) > $@

src/tizen/dlog.rs:
	mkdir -p src/tizen
	bindgen $(SYSROOT)/usr/include/dlog/dlog.h \
	                                           --whitelist-function 'dlog_.*' \
                                                   --whitelist-type "dlog_.*" \
        					   --whitelist-var "DLOG_.*" \
						   -- --sysroot=$(SYSROOT) > $@

#src/tizen/evas.rs:
#	mkdir -p src/tizen
#	bindgen $(SYSROOT)/usr/include/evas-1/Evas.h \
#	                                           --whitelist-function 'evas_.*' \
#                                                   --whitelist-type "evas_.*" \
#        					   --whitelist-var "EVAS_.*" \
#						   -- --sysroot=$(SYSROOT) \
#						      -I $(SYSROOT)/usr/include/evas-1 \
#						      -I $(SYSROOT)/usr/include/efl-1 \
#						      -I $(SYSROOT)/usr/include/eina-1 \
#						      -I $(SYSROOT)/usr/include/eina-1/eina \
#						      -I $(SYSROOT)/usr/include/eo-1  \
						      -I $(SYSROOT)/usr/include/emile-1 > $@

src/tizen/elementary.rs:
	bindgen $(SYSROOT)/usr/include/elementary-1/Elementary.h \
	                                           --whitelist-function 'evas_.*' \
                                                   --whitelist-type "Evas_.*" \
        					   --whitelist-var "EVAS_.*" \
	                                           --whitelist-function 'elm_.*' \
                                                   --whitelist-type "Elm_.*" \
        					   --whitelist-var "ELM_.*" \
						   -- --sysroot=$(SYSROOT) \
						   -I $(SYSROOT)/usr/include/efl-1 \
						   -I $(SYSROOT)/usr/include/eina-1 \
						   -I $(SYSROOT)/usr/include/eina-1/eina \
						   -I $(SYSROOT)/usr/include/eet-1 \
						   -I $(SYSROOT)/usr/include/emile-1 \
						   -I $(SYSROOT)/usr/include/evas-1 \
						   -I $(SYSROOT)/usr/include/eo-1 \
						   -I $(SYSROOT)/usr/include/ecore-1 \
						   -I $(SYSROOT)/usr/include/ecore-evas-1 \
						   -I $(SYSROOT)/usr/include/ecore-file-1 \
						   -I $(SYSROOT)/usr/include/ecore-file-1 \
						   -I $(SYSROOT)/usr/include/ecore-input-1 \
						   -I $(SYSROOT)/usr/include/ecore-imf-1 \
						   -I $(SYSROOT)/usr/include/ecore-con-1 \
						   -I $(SYSROOT)/usr/include/edje-1 \
						   -I $(SYSROOT)/usr/include/eldbus-1 \
						   -I $(SYSROOT)/usr/include/efreet-1 \
						   -I $(SYSROOT)/usr/include/ethumb-client-1 \
						   -I $(SYSROOT)/usr/include/ethumb-1 \
						   -I $(SYSROOT)/usr/include/elementary-1 > $@

clean:
	rm -rf Debug Release src/tizen
	cargo clean

.PHONY: run debug release pakage-debug package-release install-debug install-release clean
